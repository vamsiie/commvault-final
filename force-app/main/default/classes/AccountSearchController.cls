/****************************************************
Apex Class: AccountSearchController
TestClass: AccountSearchControllerTest
Description: To get the Accounts from the target org.
 ****************************************************/

public with sharing class AccountSearchController {
    public AccountSearchController() {

    }
    @AuraEnabled(cacheable=true)
    //Make the callout to org2 and fetch the data only related to the current user.
    public static String getpostSAML(String accountName){
        String returnValue = 'Failed. Please contact your system administrator';
        User u = [SELECT Id, Org2_User_Name__c from User where Id = : Userinfo.getUserId()];
        //Checking whetehr the user is available in org2
        if(u.Org2_User_Name__c!=null && u.Org2_User_Name__c!=''){
                //Authorizing Org2 using SAML Bearer Authorization. 
                //We can do authorization on behalf of any user by passing the user name in taret org.
                //We used OAuth 2.0 SAML Bearer Assertion Flow for Previously Authorized Apps
                SAMLBearerAssertion obj = new SAMLBearerAssertion(u.Org2_User_Name__c);
                httpResponse resp = obj.getpostSAML();
                if(resp.getStatusCode() == 200){
                Map<String,object> resJson = (Map<string,object>)System.JSON.deserializeUntyped(resp.getBody());
                string accessToken = string.valueOf(resJson.get('access_token'));
                system.debug('accessToken +'+ '@'+accessToken+'@');
                system.debug('instance_url +'+ string.valueOf(resJson.get('instance_url')));
                http h = new http();
                httpRequest req = new httpRequest();
                req.setHeader('Content-Type','application/json');
                req.setHeader('Authorization','Bearer '+accessToken);
                req.setMethod('GET');
                String qStr = 'https://brave-raccoon-144928-dev-ed.my.salesforce.com/services/data/v45.0/query/?q=SELECT+Id,Name,BillingCountry,(SELECT+Id+from+Contacts),(SELECT+Id+from+Opportunities+where+isClosed=false)+from+Account+where+Name+LIKE'+EncodingUtil.urlEncode('\''+'%'+accountName+'%'+'\'','UTF-8');
                req.setEndpoint(qStr);
                httpResponse res = h.send(req);
                System.debug('\n\nRESPONSE: ' + res.getBody());
                System.debug('\n\nRESPONSE: ' + res.getStatusCode() + '  ' + res.getStatus());
                return  res.getBody();
            }
            else{
                returnValue =  'Authorization issue';
            }
            
        }
        else{
            returnValue =  'No user available in Org2';
        }
        return returnValue;
        

    }
}